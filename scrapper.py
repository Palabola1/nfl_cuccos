import numpy as np
import pandas as pd
import os
import zipfile
from kaggle.competitions import nflrush

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))
            
env = nflrush.make_env()
os.makedirs("/kaggle/working/exports", exist_ok = True)
index=0
for(test_df, sample_prediction_df) in env.iter_test():
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        #print('test df begin')
        #print(test_df)
        #print('test df end')
        #print('sample pred df begin')
        #print(sample_prediction_df)
        #print('sample pred df end')
        print("iter {}".format(index))
        test_df.to_csv("/kaggle/working/exports/testdf{}.csv".format(index))
        sample_prediction_df.to_csv("/kaggle/working/exports/samplepred{}.csv".format(index))
        index = index + 1
        env.predict(sample_prediction_df)
print('creating zip')
zipf = zipfile.ZipFile('/kaggle/working/scrapper.zip', 'w', zipfile.ZIP_DEFLATED)
zipdir('/kaggle/working/exports', zipf)
zipf.close()
print('zip created')

