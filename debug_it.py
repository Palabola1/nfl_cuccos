from nfl_predict import *

train_df = pd.read_csv("train.csv", low_memory = False)

train_df.head()
cat_features = []
for col in train_df.columns:
    if train_df[col].dtype =='object':
        cat_features.append((col, len(train_df[col].unique())))

cat_features

cleaner = data_cleaner(train_df)
train_df = cleaner.clean()
sm = string_mapper(train_df)
