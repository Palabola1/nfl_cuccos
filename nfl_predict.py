# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 
# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list all files under the input directory

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import math
import datetime
import random
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras.utils import plot_model
from sklearn.preprocessing import StandardScaler
import os
import platform
import time
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from string import punctuation

def null_nan(val):
    if math.isnan(val):
        return 0.0
    return val

class train_data_wrapper():
    def __init__(self, train_df):
        self.df_ = train_df
        self.numpy_array_ = train_df.to_numpy()
        idx = 0
        for col in self.df_.columns:
            setattr(self, col, idx)
            idx = idx + 1
    def numpy(self):
        return self.numpy_array_


# helpers to map specific strings to integers
# might be a better solution to use Enumration layers instead
class string_mapper():
    def preprocess_personnel_record(rec):
        return [ [role, int(count)] for count, role in [ x.strip().split(' ') for x in rec.split(',') ] ]

    def __init__(self, train_df):
        self.offense_values_ = sorted([a for a in { a for pr in [ string_mapper.preprocess_personnel_record(rec) for rec in  train_df['OffensePersonnel'].unique() ] for a,b in pr}])
        self.offense_dict_ = dict(zip(self.offense_values_, range(1, len(self.offense_values_)+1)))
        self.defense_values_ = sorted([a for a in { a for pr in [ string_mapper.preprocess_personnel_record(rec) for rec in  train_df['DefensePersonnel'].unique() ] for a,b in pr}])
        self.defense_dict_ = dict(zip(self.defense_values_, range(1, len(self.defense_values_)+1)))
        self.offense_formation_values_ = train_df['OffenseFormation'].unique()
        self.offense_formation_dict_ = dict(zip(self.offense_formation_values_, range(1, len(self.offense_formation_values_)+1)))
        self.role_values_ = train_df['Position'].unique()
        self.role_dict_ = dict(zip(self.role_values_, range(1, len(self.role_values_)+1)))
        self.nflid_values_ = train_df['NflId'].unique()
        self.nflid_dict_ = dict(zip(self.nflid_values_, range(1, len(self.nflid_values_)+1)))
        self.teamabbr_values_ = sorted({ x for x in sorted(train_df['HomeTeamAbbr'].unique()) + sorted(train_df['VisitorTeamAbbr'].unique()) + sorted(train_df['PossessionTeam'].unique()) })
        self.teamabbr_dict_ = dict(zip(self.teamabbr_values_, range(1, len(self.teamabbr_values_)+1)))

    def offense_roles(self, argument):
        return self.offense_dict_.get(argument, 0)

    def defense_roles(self, argument):
        return self.defense_dict_.get(argument, 0)

    def offense_formations(self, argument):
        return self.offense_formation_dict_.get(argument, 0)

    def team_id(self, argument):
        return self.teamabbr_dict_.get(argument, 0)

    def role_id(self, argument):
        return self.role_dict_.get(argument, 0)

    def num_offense_defense_values(self):
        return max(len(self.offense_values_), len(self.defense_values_))

    def NFL_id(self, argument):
        return self.nflid_dict_.get(argument, 0)

    def GameTimeToSec(self, argument):
        gametime = datetime.datetime.strptime(argument, '%M:%S:%f')
        return (gametime.minute * 60 + gametime.second)


class data_cleaner():
    def append_replacement(self, alias1, alias2):
        if alias1 < alias2:
            self.replacements_[alias1] = alias2
        else:
            self.replacements_[alias2] = alias1
        return True

    def __init__(self, tr):
        self.hometeamabbrs_ = sorted(tr['HomeTeamAbbr'].unique())
        self.visitorteamabbrs_ = sorted(tr['VisitorTeamAbbr'].unique())
        self.possessionteams_ = sorted(tr['PossessionTeam'].unique())
        self.replacements_ = {}
        all(map(lambda x: self.append_replacement(x, x), self.visitorteamabbrs_))
        all(map(lambda x: self.append_replacement(x, x), self.hometeamabbrs_))
        all(map(lambda x: self.append_replacement(x, x), self.possessionteams_))
        all(map(lambda x: self.append_replacement(x[0], x[1]), filter(lambda x: x[0] != x[1], zip(self.visitorteamabbrs_, self.possessionteams_))))
        all(map(lambda x: self.append_replacement(x[0], x[1]), filter(lambda x: x[0] != x[1], zip(self.hometeamabbrs_, self.visitorteamabbrs_))))
        all(map(lambda x: self.append_replacement(x[0], x[1]), filter(lambda x: x[0] != x[1], zip(self.hometeamabbrs_, self.possessionteams_))))
        self.train_df_ = tr

    def clean_StadiumType(txt):
        if pd.isna(txt):
            return np.nan
        txt = txt.lower()
        txt = ''.join([c for c in txt if c not in punctuation])
        txt = txt.strip()
        txt = txt.replace('outside', 'outdoor')
        txt = txt.replace('outdor', 'outdoor')
        txt = txt.replace('outddors', 'outdoor')
        txt = txt.replace('outdoors', 'outdoor')
        txt = txt.replace('oudoor', 'outdoor')
        txt = txt.replace('ourdoor', 'outdoor')
        txt = txt.replace('indoors', 'indoor')
        txt = txt.replace('retractable', 'rtr.')
        return txt
    def clean_Turf(tr):
        Turf = {'Field Turf':'Artificial', 'A-Turf Titan':'Artificial', 'Grass':'Natural', 'UBU Sports Speed S5-M':'Artificial', 
                'Artificial':'Artificial', 'DD GrassMaster':'Artificial', 'Natural Grass':'Natural', 
                'UBU Speed Series-S5-M':'Artificial', 'FieldTurf':'Artificial', 'FieldTurf 360':'Artificial', 'Natural grass':'Natural', 'grass':'Natural', 
                'Natural':'Natural', 'Artifical':'Artificial', 'FieldTurf360':'Artificial', 'Naturall Grass':'Natural', 'Field turf':'Artificial', 
                'SISGrass':'Artificial', 'Twenty-Four/Seven Turf':'Artificial', 'natural grass':'Natural'}
        return tr.map(Turf)
    def clean_TeamAbbrev(self, tr):
        return tr.map(self.replacements_)
    def clean_GameClock(txt):
        txt = txt.split(':')
        ans = int(txt[0])*60 + int(txt[1]) + int(txt[2])/60
        return ans

    def clean(self, tr):
        #tr = self.train_df_
        tr['StadiumType'] = tr['StadiumType'].apply(data_cleaner.clean_StadiumType)
        tr['Turf'] = data_cleaner.clean_Turf(tr['Turf'])
        tr['PossessionTeam'] = self.clean_TeamAbbrev(tr['PossessionTeam'])
        tr['HomeTeamAbbr'] = self.clean_TeamAbbrev(tr['HomeTeamAbbr'])
        tr['VisitorTeamAbbr'] = self.clean_TeamAbbrev(tr['VisitorTeamAbbr'])
        return tr

    def transform(self, mapper, tr):
        #tr = self.train_df_
        tr['Position'] = tr['Position'].apply(mapper.role_id)
        tr['FieldPosition'] = tr['FieldPosition'].apply(mapper.team_id)
        tr['PossessionTeam'] = tr['PossessionTeam'].apply(mapper.team_id)
        tr['HomeTeamAbbr'] = tr['HomeTeamAbbr'].apply(mapper.team_id)
        tr['VisitorTeamAbbr'] = tr['VisitorTeamAbbr'].apply(mapper.team_id)
        tr['GameClock'] = tr['GameClock'].apply(mapper.GameTimeToSec)

        tr['HomePossession'] = tr['PossessionTeam'] == tr['HomeTeamAbbr']
        # tr['Field_eq_Possession'] = tr['FieldPosition'] == tr['PossessionTeam']
        # tr['HomeField'] = tr['FieldPosition'] == tr['HomeTeamAbbr']
        # tr = pd.concat([tr.drop(['OffenseFormation'], axis=1), pd.get_dummies(tr['OffenseFormation'], prefix='Formation')], axis=1)
        # tr['GameClock'] = tr['GameClock'].apply(data_cleaner.clean_GameClock)
        return tr


class perf_counter():
    def __init__(self, debug):
        self.ipc_ = time.perf_counter()
        self.pc_ = self.ipc_
        self.debug_ = debug
    def elapsed(self):
        pc = time.perf_counter()
        d = pc - self.pc_
        td = pc - self.ipc_
        self.pc_ = pc
        return d, td
    def report(self, tag):
        if self.debug_:
            d, td = self.elapsed()
            print("{}: time since last measurement: {:.2f}s [total: {:.2f}s]".format(tag, d, td))

timer = None

def eswish(x):
    beta = 1.0 # 0.5 jó!
    return beta * K.sigmoid(x) * x

def eswish_control(x):
    beta = 0.5
    return beta * K.sigmoid(x) * x

def swish(x):
    return K.sigmoid(x) * x

def crps(y_true, y_pred):
    loss = K.mean((K.cumsum(y_pred, axis = 1) - y_true)**2)
    return loss
    
class DataScalerWrapper:
    def __init__(self, data):
        self.scalers_ = []
        self.size_ = len(data)
        for i in range(self.size_):
            scaler = StandardScaler()
            scaler.fit(data[i])
            self.scalers_.append(scaler)

    def transform(self, data):
        if len(data) != self.size_:
                print("Transform data shape mismatch")
                return data

        for i in range(self.size_):
            data[i] = self.scalers_[i].transform(data[i])

        return data


# globals
builders = []
best_builder = None
best_loss = 2.0

verify_yard = []
verify_data = []

g_string_mapper = None
g_data_cleaner = None
g_data_scaler = None

# extract the offense/defense formation string such as "2RD 2WR 3TE" to an int array
def extractFormationPersonnel(input, isOffense):
    global g_string_mapper
    input_array = input.split(',')
    retVal = [0.0] * (g_string_mapper.num_offense_defense_values() + 1)
    for i in range(len(input_array)):
        idx = 0
        if len(input_array[i]) > 4:
            idx = 1
        num = int(input_array[i][idx])
        if isOffense:
            pos = g_string_mapper.offense_roles(input_array[i][2+idx:])
        else:
            pos = g_string_mapper.defense_roles(input_array[i][2+idx:])
        retVal[pos] = num
    return retVal

# flips the orientation/direction vertically
def VerticalFlip(x):
    retVal = 360.0 - x
    if retVal == 360:
        return 0.0

    return retVal

def VisualizePlay(dw, idx):

    input_numpy = dw.numpy()[idx:idx+22]
    row = input_numpy[0]
    isHomeAttacking = row[dw.PossessionTeam] == row[dw.HomeTeamAbbr]

    x_arr = []
    y_arr = []
    x1_arr = []
    y1_arr = []
    rush_x = 0.0
    rush_y = 0.0
    QB_x = 0.0
    QB_y = 0.0
    for temp_row in input_numpy:
        if temp_row[dw.NflId] == temp_row[dw.NflIdRusher]:
            rush_x = temp_row[dw.X]
            rush_y = temp_row[dw.Y]
        elif temp_row[dw.Position] == g_string_mapper.role_id("QB"):
            QB_x = temp_row[dw.X]
            QB_y = temp_row[dw.Y]
        elif (not isHomeAttacking and temp_row[dw.Team] == 'away') or (isHomeAttacking and temp_row[dw.Team] == 'home'):
            x_arr.append(temp_row[dw.X])
            y_arr.append(temp_row[dw.Y])
        else:
            x1_arr.append(temp_row[dw.X])
            y1_arr.append(temp_row[dw.Y])

    plt.title(str(row[dw.Yards]))
    plt.scatter(x_arr, y_arr, c='red')
    plt.scatter(x1_arr, y1_arr, c='blue')
    plt.scatter(rush_x, rush_y, c='black')
    plt.scatter(QB_x, QB_y, c='gray')

    rows_per_play = 22
    for temp_row in input_numpy:
        x1 = temp_row[dw.X]
        y1 = temp_row[dw.Y]
        x2 = x1
        y2 = y1
        x2 += math.sin(temp_row[dw.Dir]) * (temp_row[dw.S] + temp_row[dw.A])
        y2 += math.cos(temp_row[dw.Dir]) * (temp_row[dw.S] + temp_row[dw.A])
        plt.plot([x1, x2], [y1, y2])

    yardline = 110 - row[dw.YardLine]

    plt.plot([yardline, yardline], [10, 40], linestyle='dashed')

    yardline += row[dw.Yards]

    plt.plot([yardline, yardline], [10, 40], linestyle='dashed')

    #plt.xlim(0, 120)
    #plt.ylim(0, 55)

    # Add a legend
    plt.legend()

    # Show the plot
    plt.show()


# extracts play specific information from the input, e.g. line of scrimmage, quarter 
def ProcessPlayData(dw, row):
    global g_string_mapper
    play_data = []

    isHomeAttacking = row[dw.HomePossession]
  
    play_data.append(row[dw.YardLine]) # 6% correlation
    play_data.append(row[dw.Quarter]) # 1% dorrelation
    
    #gametime = datetime.datetime.strptime(row[dw.GameClock], '%M:%S:%f')
    play_data.append(row[dw.GameClock]) # 3% correlation 

    play_data.append(row[dw.Down] / 3.0) # -2% correlation
    play_data.append(row[dw.Distance] / 10.0) # has correlaion
    play_data.append(row[dw.Distance]/(5.0 - row[dw.Down]) / 3.3)
    
    # the point difference between the currently attacking vs defending team
    if isHomeAttacking:  # seemingly no correlation
        play_data.append((row[dw.HomeScoreBeforePlay]-row[dw.VisitorScoreBeforePlay]) / 10.0)
    else:
        play_data.append((row[dw.VisitorScoreBeforePlay]-row[dw.HomeScoreBeforePlay]) / 10.0)
    
    play_data.append(g_string_mapper.offense_formations(row[dw.OffenseFormation]) / 7.0) # matters a bit

    offense_array = extractFormationPersonnel(row[dw.OffensePersonnel], True)

    # run distance seems to depend on the number of WR, typically 2 or 3
    # the other positions doesn't have such significance
    play_data.append(offense_array[g_string_mapper.offense_roles("WR")] / 4.0)
    
    play_data.append(row[dw.DefendersInTheBox] / 11.0) #matters

    offense_array = extractFormationPersonnel(row[dw.DefensePersonnel], False)
    
    # sama as opffense line "DB" position semms to have greater infomrmation value
    play_data.append(offense_array[g_string_mapper.defense_roles("DB")] / 11.0)

    # not sure if it matters
    handoff = datetime.datetime.strptime(row[dw.TimeHandoff], '%Y-%m-%dT%H:%M:%S.%fZ')
    snap = datetime.datetime.strptime(row[dw.TimeSnap], '%Y-%m-%dT%H:%M:%S.%fZ')
    timeElapsed = (handoff-snap).total_seconds()
    play_data.append(timeElapsed)
    
    # the team ID of the offense/defense team
    if isHomeAttacking: 
        play_data.append(row[dw.HomeTeamAbbr]) 
        play_data.append(row[dw.VisitorTeamAbbr])
    else:  
        play_data.append(row[dw.VisitorTeamAbbr]) 
        play_data.append(row[dw.HomeTeamAbbr])
            
    return play_data

# extract data specific to 1 player, such as (X,Y), speed, acceleration, role...
def ProcessPlayerData(dw, row):
    player_data = []
    player_data.append(null_nan((110.0-row[dw.YardLine]) - row[dw.X]))
    #player_data.append(null_nan(row[dw.Y]))
    player_data.append(null_nan(row[dw.S]))
    player_data.append(null_nan(row[dw.A]))
    player_data.append(null_nan(row[dw.PlayerWeight]))
    return player_data

def distance2D(dw, row1, row2):
    return math.sqrt((row1[dw.X]- row2[dw.X]) * (row1[dw.X] - row2[dw.X]) + (row1[dw.Y]- row2[dw.Y]) * (row1[dw.Y] - row2[dw.Y]))

def ProcessRusherData(dw, row, input_numpy):
    rusher_data = []
    rusher_data.append(row[dw.Position]) # matters!
    rusher_data.append(((110.0 - row[dw.YardLine]) - row[dw.X]))
    rusher_data.append(null_nan(row[dw.S])) # 8% correlation
    rusher_data.append(null_nan(row[dw.A])) # 16% correlation
    rusher_data.append(null_nan(row[dw.Dis])) # 5% correlation
    rusher_data.append(null_nan(row[dw.DefendersInTheBox])) # 12% correlation
    rusher_data.append(null_nan(row[dw.PlayerWeight])) # -3% correlation
    rusher_data.append(null_nan(row[dw.Distance])) # 7% correlation
    #rusher_data.append(round((math.sin(row[dw.Dir])),2)) # -1% correlation
    #rusher_data.append(round(row[dw.X],2) / 120.0) # 0% corr
    #rusher_data.append(round(row[dw.Y],2) / 50.0)  # 05 corr

    handoff = datetime.datetime.strptime(row[dw.TimeHandoff], '%Y-%m-%dT%H:%M:%S.%fZ')
    snap = datetime.datetime.strptime(row[dw.TimeSnap], '%Y-%m-%dT%H:%M:%S.%fZ')
    timeElapsed = (handoff-snap).total_seconds() + 0.8

    maximum_effective_distance = math.sin(row[dw.Dir]) * (row[dw.S] + (row[dw.A]* timeElapsed/2)) * timeElapsed

    rusher_data.append(maximum_effective_distance)

    out_of_range_defenders = 0
    for row2 in input_numpy:
        if distance2D(dw, row, row2) > maximum_effective_distance:
            out_of_range_defenders += 1

    rusher_data.append(out_of_range_defenders)

    return rusher_data


# converts a few clumoms to more robust form
def preProcessData(dw, start_idx = 0):
    count = 0
    global g_string_mapper
    input_numpy = dw.numpy()[start_idx:]
    if len(input_numpy) == 0:
        return 0, 0

    playid = input_numpy[0][dw.PlayId]
    for row in input_numpy:
        if row[dw.PlayId] != playid:
            break
        count += 1
        # transform yardline to [0-100] interval
        # 50 is the exact center of the field
        if row[dw.YardLine] != 50:
            if row[dw.PossessionTeam] == row[dw.FieldPosition]:
                row[dw.YardLine] = (100.0 - row[dw.YardLine])

        # input data shifted 90 deg for 2017
        # https://www.kaggle.com/c/nfl-big-data-bowl-2020/discussion/113384#latest-653467
        if row[dw.Season] == 2017:
            row[dw.Orientation] = (row[dw.Orientation] + 90) % 360

        # flip the "board" vertically depending on play direction
        if  row[dw.PlayDirection] == "left": 
            row[dw.X]           = ((120.0 - row[dw.X]))
            row[dw.Orientation] = (VerticalFlip(row[dw.Orientation])) 
            row[dw.Dir]         = (VerticalFlip(row[dw.Dir]))

        row[dw.Orientation] = row[dw.Orientation] * math.pi / 180.0
        row[dw.Dir]         = row[dw.Dir] * math.pi / 180.0

    return playid, count

# this manages the play and player data extraction
# on top of that global values also computed here
# TODO it might be better to move these global calculations to another function idk
def ProcessOnePlay(dw, start_idx = 0):
    playid, rows_in_play = preProcessData(dw, start_idx)
    if rows_in_play <= 0:
        return tuple(), tuple(), tuple(), tuple(), -1

    play_data = []
    offense_data = []
    defense_data = []
    global g_string_mapper

    input_numpy = dw.numpy()[start_idx:start_idx+rows_in_play]
    first_row = input_numpy[0]
    play_data.extend(ProcessPlayData(dw, first_row))

    QB_dist = 0.0
    for row in input_numpy:
        tr_playid = row[dw.PlayId]
        if tr_playid != playid:
            break

        if row[dw.NflId] == row[dw.NflIdRusher]:
            rusher_data = ProcessRusherData(dw, row, input_numpy)

        # QB distance from line of scrimmage
        # also significant
        if row[dw.Position] == g_string_mapper.role_id("QB"): # QB
            QB_dist = round(((110.0-row[dw.YardLine]) - row[dw.X]),1)

        # player specific data different for each row
        # isAttacker
        player_data = ProcessPlayerData(dw,row)

        isHomeAttacking = row[dw.PossessionTeam] == row[dw.HomeTeamAbbr]
        if (not isHomeAttacking and row[dw.Team] == 'away') or (isHomeAttacking and row[dw.Team] == 'home'):
            offense_data.extend(player_data)
        else:
            defense_data.extend(player_data)

    play_data.append(QB_dist)

    # error correction, bc the model dies from a NAN
    for j in range(len(play_data)-1):
        play_data[j] = null_nan(play_data[j])

    # fill the x_true array
    yard_array_len = 199
    zeroes = yard_array_len
    if hasattr(dw, "Yards"):
        zeroes = max(0, min(first_row[dw.Yards] + 99, yard_array_len))
    yard_array = [0.0] * zeroes + [1.0] * (yard_array_len - zeroes)

    return tuple(play_data), tuple(offense_data), tuple(defense_data), tuple(rusher_data), tuple(yard_array), rows_in_play

# the control model, used on the earliest stage of development
# it scored around 0.0145 on competition data
def BuildControlModel(test_data, test_true, num_epochs, batch):
    
    input = keras.layers.Input(shape=test_data[0][0].shape)
    input1 = keras.layers.Flatten()(input)

    input2 = keras.layers.Input(shape=(test_data[1][0].shape))
    input21 = keras.layers.Flatten()(input2)

    input3 = keras.layers.Input(shape=(test_data[2][0].shape))
    input31 = keras.layers.Flatten()(input3)

    input4 = keras.layers.Input(shape=(test_data[3][0].shape))
    input41 = keras.layers.Flatten()(input4)

    concat = keras.layers.concatenate([input1,input21,input31,input41])
    concat = keras.layers.Dense(2048, activation=eswish_control)(concat)
    concat = keras.layers.Dense(199, activation='softmax')(concat)

    model = keras.models.Model(inputs=[input, input2, input3, input4], outputs=[concat])

    #model.summary()
    #plot_model(model, to_file='modelControl.png', show_shapes=True)
    
    model.compile(optimizer='adam',
                 loss=crps)

    model.fit([test_data[0], test_data[1], test_data[2], test_data[3]], test_true, epochs=num_epochs, batch_size=batch,
            validation_split = 0.1)   
    return model

class ModelBuilder():
    def __init__(self, numEpochs, batchSize, dropoutRate, normalizeBatch):
        self.dropout_rate_ = dropoutRate
        self.normalize_batch_ = normalizeBatch
        self.batchSize_ = batchSize
        self.numEpoch_ = numEpochs
    def generic_wrap_layer(self, layer):
        ly = keras.layers.Dropout(self.dropout_rate_)(layer)
        if self.normalize_batch_:
            ly = keras.layers.BatchNormalization()(ly)
        return ly
    def create_output_layer(self, input_layer):
        output_points = 199
        return keras.layers.Dense(output_points, activation='softmax')(input_layer)

    def GetConfString(self):
        return "Batch: " + str(self.batchSize_) + " dropout: " + str(round(self.dropout_rate_, 2)) + " useBatchNorm: " + str(self.normalize_batch_)

    def GetModel(self):
        return self.model_

    def GetPerformance(self):
        return self.perf_

    def PrintModel(self):
        self.model_.summary()
        plot_model(self.model_, to_file='modelControl.png', show_shapes=True)

    def BuildAndTrainModel(self, train_data, train_true, val_data, val_true):
        input = keras.layers.Input(shape=train_data[0][0].shape)
        input1 = keras.layers.Flatten()(input)
        input2 = keras.layers.Input(shape=(train_data[1][0].shape))
        input21 = keras.layers.Flatten()(input2)
        input3 = keras.layers.Input(shape=(train_data[2][0].shape))
        input31 = keras.layers.Flatten()(input3)
        input4 = keras.layers.Input(shape=(train_data[3][0].shape))
        input41 = keras.layers.Flatten()(input4)

        concat = keras.layers.concatenate([input1,input21,input31,input41])
        concat = self.generic_wrap_layer(keras.layers.Dense(2048, activation=swish)(concat))
        concat = self.generic_wrap_layer(keras.layers.Dense(1024, activation=swish)(concat))
        concat = self.generic_wrap_layer(keras.layers.Dense(512, activation=swish)(concat))
        concat = self.generic_wrap_layer(keras.layers.Dense(256, activation=swish)(concat))

        output = self.create_output_layer(concat)

        # build the model
        self.model_ = keras.models.Model(inputs=[input, input2, input3, input4], outputs=[output])

        initial_learning_rate = 0.001
        lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate,
            decay_steps=100,
            decay_rate=0.90,
            staircase=True)

        self.model_.compile(optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
                    loss=crps)

        es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)

        filepath="weights.best.hdf5"
        checkpoint = keras.callbacks.ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=True, mode='min')

        #log_dir="logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        #tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        self.model_.fit([train_data[0], train_data[1], train_data[2], train_data[3]], train_true, epochs=self.numEpoch_, batch_size=self.batchSize_,
                callbacks=[es, checkpoint],
                validation_split = 0.105) 

        # load the best solution
        self.model_.load_weights("weights.best.hdf5")

        self.perf_ = self.model_.evaluate(val_data, val_true, batch_size=1)
        print(self.perf_)
    
        return (self.model_, self.perf_)

def train_my_model(train_data, isDebug):
    global verify_play
    global verify_yard
    global timer

    play_container = [] 

    data = train_data_wrapper(train_data)
    numpy_array = data.numpy()
    idx = 0

    print(numpy_array.shape)

    while idx < numpy_array.shape[0]:
        (play_data, off_data, def_data, rusher_data, yard_array, rows_processed) = ProcessOnePlay(data, idx)

        #VisualizePlay(data, idx)

        play_container.append((play_data, off_data, def_data, rusher_data, yard_array))

        idx += rows_processed

    timer.report("all plays processed")

    random.shuffle(play_container)

    # manually select a validation set for later verification
    validation_data_size = 2000

    rand = random.randrange(len(play_container) - validation_data_size)
    verify_data = play_container[rand:rand+validation_data_size]
    verify_play = [[x[i] for x in verify_data] for i in range(4)]
    verify_yard = [x[4] for x in verify_data]

    play_container = play_container[:rand] + play_container[rand+validation_data_size:]
    train_play = [[x[i] for x in play_container] for i in range(4)]
    train_yard = [x[4] for x in play_container]

    global g_data_scaler
    g_data_scaler = DataScalerWrapper(train_play)

    train_play = g_data_scaler.transform(train_play)
    verify_play = g_data_scaler.transform(verify_play)

    # convert [] to numpy bc of keras
    npPlay = [np.asarray(train_play[i], dtype=np.float32) for i in range(4)]
    npYard = np.asarray(train_yard, dtype=np.float32) 

    valPlay = [np.asarray(verify_play[i], dtype=np.float32) for i in range(4)]
    valYard = np.asarray(verify_yard, dtype=np.float32) 

    print("Input shapes")
    print(npPlay[0].shape)
    print(npPlay[1].shape)
    print(npPlay[2].shape)
    print(npPlay[3].shape)

    timer.report("start training models")

    for i in range(30):
        timer.report("training model #{}".format(i+1))
        builder = ModelBuilder(250, random.randrange(100, 300, 1), random.uniform(0.05, 0.45), True)
        builder.BuildAndTrainModel(npPlay, npYard, valPlay, valYard)
        builders.append(builder)

        global best_builder
        global best_loss
        if  builder.GetPerformance() < best_loss:
            best_loss = builder.GetPerformance() 
            best_builder = builder
            print("New best found: {}".format(best_loss))

        timer.report("model #{} trained".format(i+1))
    
    timer.report("models trained")

    return

# correct/optimize the output
def correct_output_swish(sample):

    # theoretically thse values always should have these values
    sample[0,0] = 0.0
    sample[0,198] = 1.0
    
    # hack a little bit
    # removes the approximation noise like 0.00001 probability
    # might be a good idea might be a bad idea idk
    for i in range(len(sample[0])):
        if sample[0,i] < 0.01:
            sample[0,i] = 0.0
        if sample[0,i] > 0.99:
            sample[0,i] = 1.0

    # convert swish to CPRS
    for i in range(1, len(sample[0])):
        if sample[0, i-1] > sample[0, i]:
            sample[0, i] = sample[0, i-1]
    return sample

# correct/optimize the output
def correct_output_softmax(sample):

    # theoretically thse values always should have these values
    sample[0] = 0.0
    sample[198] = 1.0
    
    # convert softmax to CPRS
    for i in range(1, len(sample)):
        sample[i] += sample[i-1]
    
    # hack a little bit
    # removes the approximation noise like 0.00001 probability
    # might be a good idea might be a bad idea idk
    for i in range(len(sample)):
        if sample[i] < 0.01:
            sample[i] = 0.0
        if sample[i] > 0.99:
            sample[i] = 1.0
    return sample
    
def generic_predictor(model, play_data, offense_data, defense_data, rusher_data):
    timer.report("start prediction")
    sample = model.predict([np.asarray(play_data,dtype=np.float32), np.asarray(offense_data,dtype=np.float32), np.asarray(defense_data,dtype=np.float32), np.asarray(rusher_data,dtype=np.float32)])
    timer.report("prediction done")

    for s in sample:
        correct_output_softmax(s)
    timer.report("corrections done")

    return sample

# competition predictor
def make_my_predictions(test_data, sample_df):

    global g_data_cleaner
    test_data = g_data_cleaner.clean(test_data)
    global g_string_mapper
    test_data = g_data_cleaner.transform(g_string_mapper, test_data)

    data = train_data_wrapper(test_data)

    (play_data, offense_data, defense_data, rusher_data, yard_array, rows_processed) = ProcessOnePlay(data)
  
    global g_data_scaler
    play_data = g_data_scaler.transform([[play_data], [offense_data], [defense_data], [rusher_data]])

    sample = generic_predictor(best_builder.GetModel(), play_data[0], play_data[1], play_data[2], play_data[3])

    # competition system requires DataFrame type
    sample_df.iloc[0] = sample[0]
    
    return sample_df

def CPRS(array_true, array_pred):
    error = 0.0
    for i in range(len(array_true)):
        error += (array_true[i] - array_pred[i]) * (array_true[i] - array_pred[i])
    error /= len(array_true)
    return error

def test_prediction_for_model(model):
    timer.report("start prediction test for model")
    sum_error = 0.0
    samples = generic_predictor(model, verify_play[0], verify_play[1], verify_play[2], verify_play[3])
    for vd, s in zip(verify_yard, samples):
        error = CPRS(vd, s)
        sum_error += error

    sum_error /= len(verify_play[0])
    timer.report("prediciton test for model done")
    return sum_error

def sortMe(e):
    return e.GetPerformance()

def perdiction_test():
    timer.report("start prediction test")
    sum_error = test_prediction_for_model(best_builder.GetModel())
    print("Best real loss: " + str(sum_error) + "conf: " + best_builder.GetConfString())

    builders.sort(key=sortMe)
    for builder in builders:
        sum_error = test_prediction_for_model(builder.GetModel())
        print("Test loss: " + str(sum_error) + "conf: " + builder.GetConfString())
    timer.report("prediction test completed")

import seaborn as sns
def DataAnalitics(data):
    return 
    data.drop(["WindDirection","WindSpeed","Temperature","GameWeather","StadiumType","Humidity"],axis=1,inplace=True)
  
    #dw = data[data.NflId == data.NflIdRusher]

    for idx, row in data.iterrows():
    

        # input data shifted 90 deg for 2017
        # https://www.kaggle.com/c/nfl-big-data-bowl-2020/discussion/113384#latest-653467
        if row["Season"] == 2017:
            row["Orientation"] = (row["Orientation"] + 90) % 360

        # flip the "board" vertically depending on play direction
        if  row["PlayDirection"] == "left": 
            row["X"]           = ((120.0 - row["X"]))
            row["Orientation"] = (VerticalFlip(row["Orientation"])) 
            row["Dir"]         = (VerticalFlip(row["Dir"]))

        row["Orientation"] = row["Orientation"] * math.pi / 180.0
        row["Dir"]         = row["Dir"] * math.pi / 180.0


    data.drop(["PlayDirection", "HomePossession","PlayLength", "Down", "YardLine"],axis=1,inplace=True)

    f,ax = plt.subplots(figsize=(12,10))
    sns.heatmap(data.corr(),annot=True, linewidths=.1, fmt='.0%', ax=ax)

    plt.show()

def main(isDebug, fname, lowmem, profile):
    global timer
    timer = perf_counter(profile)
    timer.report("enter main")
    if isDebug == False:
        from kaggle.competitions import nflrush
        env = nflrush.make_env()
    timer.report("start csv load")
    train_df = pd.read_csv(fname, low_memory=lowmem)
    timer.report("csv loaded")

    global g_data_cleaner
    g_data_cleaner = data_cleaner(train_df)
    train_df = g_data_cleaner.clean(train_df)
    global g_string_mapper
    g_string_mapper = string_mapper(train_df)
    train_df = g_data_cleaner.transform(g_string_mapper, train_df)

    timer.report("data cleaned")
    DataAnalitics(train_df)
    train_my_model(train_df, isDebug)
    timer.report("training complete")

    if isDebug == False:
        for (test_df, sample_prediction_df) in env.iter_test():
            predictions_df = make_my_predictions(test_df, sample_prediction_df)
            env.predict(predictions_df)

        env.write_submission_file()
    else:
        perdiction_test()
    timer.report("main done")

if __name__ == "__main__":
    main(False, '/kaggle/input/nfl-big-data-bowl-2020/train.csv', False, False)

