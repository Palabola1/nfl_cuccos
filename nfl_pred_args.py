import nfl_predict
import argparse
import random
import numpy.random as nprandom
import tensorflow as tf

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='predict plays')
    parser.add_argument('--filename', dest='filename', default='/kaggle/input/nfl-big-data-bowl-2020/train.csv')
    parser.add_argument('--debug', dest='debug', action='store_const', const=True, default=False)
    parser.add_argument('--profile', dest='profile', action='store_const', const=True, default=False)
    parser.add_argument('--lowmem', dest='low_mem', action='store_const', const=True, default=False)
    parser.add_argument('--seed', dest='seed', default=None)
    args = parser.parse_args()

    if args.seed:
        s = int(args.seed)
        random.seed(s)
        nprandom.seed(s)
        tf.random.set_seed(s)
    nfl_predict.main(args.debug, args.filename, args.low_mem, args.profile)

