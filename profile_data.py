#! /bin/env python
import pandas as pd
import pandas_profiling

df = pd.read_csv('train.csv', low_memory=False)
prof1 = df.profile_report(style={'full_width':True})
prof1.to_file(output_file="output.html")
